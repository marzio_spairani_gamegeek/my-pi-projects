import RPi.GPIO as GPIO
import time


GPIO.setmode (GPIO.BCM)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)
GPIO.setup(23, GPIO.IN)


while True:
    
    if GPIO.input(23):
    
        GPIO.output(24, True) 

    
        GPIO.output(25, True) 
        
        time.sleep(1)
        
        GPIO.output(24, False)
       

        GPIO.output(25, False)
       
        time.sleep(1) 
    
    else: 
        GPIO.output(24, False)
        GPIO.output(25, False)
 
        